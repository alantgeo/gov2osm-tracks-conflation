# gov2osm Tracks Conflation

This project sources government track data ([`highway=track`](http://wiki.openstreetmap.org/wiki/Tag:highway=track)), converts it into the OSM tagging schema, and conflates with existing data in OSM to identify differences.

## Sources

### ACT

[ACTmapi data](https://www.actmapi.act.gov.au/download.html) from _www.ACTmapi.act.gov.au ©Australian Capital Territory_. [CC BY 4.0](https://www.actmapi.act.gov.au/terms.html) with a [waiver](https://wiki.openstreetmap.org/wiki/File:ACT_Government_20180314_Permission_to_Incorporate_CCBY_data_into_OSM.PDF).

This data actually represents the track corridor but it's good enough for conflation. Only the `name` tag is included.

### VIC

[Vicmap Transport - Roads](https://www.land.vic.gov.au/maps-and-spatial/spatial-data/vicmap-catalogue/vicmap-transport) _© State of Victoria (Department of Environment, Land, Water and Planning)_. [CC BY 4.0](https://www.delwp.vic.gov.au/copyright) with a [waiver](https://wiki.openstreetmap.org/wiki/File:Vicmap_CCBYPermission_OSM_Final_Jan2018_Ltr.pdf).

Appendix B section 8.3 of the data specification defines the Road Class Code Definitions as:

- 6 **Track 2-Wheel Drive** _Unimproved roads which are generally only passable in two wheel drive vehicles during fair weather and are used predominately by local traffic. Also included are driveways regardless of construction._
- 7 **Track 4-Wheel Drive** _Unimproved roads which are generally only passable with four wheel drive vehicles._

These two classes are used to filter out tracks.

### NSW

[NPWS Asset Infrastructure Road Section](https://datasets.seed.nsw.gov.au/dataset/asset-infrastructure-road-section) _© State Government of NSW and Department of Planning, Industry and Environment 2017_. CC BY 4.0 with a [waiver](https://wiki.openstreetmap.org/wiki/File:OpenStreetMap_OEH_CCBYPermission.pdf).

[NSW Transport Theme - Classified Fire Trail](https://portal.spatial.nsw.gov.au/portal/home/item.html?id=fe9e02f814e345bba2d9e77081faca6c) _© Spatial Services 2021._ CC BY 4.0 with a [waiver](https://wiki.openstreetmap.org/wiki/File:SpatialServices_NSW_OSM_Waiver2_completed.pdf).

### SA

[Department for Infrastructure and Transport Roads](https://data.sa.gov.au/data/dataset/roads) CC BY 4.0 with a [waiver](https://wiki.openstreetmap.org/wiki/File:Data.sa.gov.au_CCBYPermission_Open_Street_Map.pdf).

### WA

[Main Roads Western Australia Road Network](https://catalogue.data.wa.gov.au/dataset/mrwa-road-network) CC BY 4.0 with a [waiver](https://wiki.openstreetmap.org/wiki/File:OSM_CCBY_Consent-MainRoadsWA.pdf).

### TAS

CC BY 4.0 without a waiver

## MapRoulette

The conflation process outputs tracks we may be missing from OSM as a MapRoulette challenge:

- [https://maproulette.org/browse/projects/45979](https://maproulette.org/browse/projects/45979)

### Description
Tracks (highway=track) identified from government data were compared with existing tracks in OSM, and potential missing tracks in OSM have been flagged for review. To be added to OSM if confirmed through a secondary source like satellite/aerial imagery or street level imagery.

#### Instructions
See if the track highlighted can be confirmed with a secondary imagery source, and if so then it can be mapped as `highway=track`.

If using mapwith.ai to add the missing track using the AI generated geometry, take care to review the proposed classification.
