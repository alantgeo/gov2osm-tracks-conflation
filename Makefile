### OSM

build/australia.osm.pbf:
	# wget -O $@ http://download.openstreetmap.fr/extracts/oceania/australia.osm.pbf
	wget -O $@ https://download.geofabrik.de/australia-oceania/australia-latest.osm.pbf

build/australia-roads.osm.pbf: build/australia.osm.pbf
	osmium tags-filter --overwrite --output=$@ $< w/highway

osmPg: build/australia-roads.osm.pbf
	ogr2ogr -f PostgreSQL -unsetFid -nln osm -t_srs 'EPSG:3857' -lco OVERWRITE=YES -lco GEOMETRY_NAME=geom PG: $< lines

### GOV

govPg: govPgACT govPgNSW govPgNT govPgQLD govPgSA govPgVIC

### ACT

build/ACT_data.zip:
	wget -O $@ https://actmapi.act.gov.au/datadownload/Geodatabase/MGA94_55/ACT_data.zip

build/ACT_data: build/ACT_data.zip
	unzip -d $@ $<

govPgACT: build/ACT_data.gdb
	psql -c 'DROP TABLE IF EXISTS gov_act;'
	ogr2ogr -f PostgreSQL -unsetFid -sql 'SELECT ROAD_NAME as name, OBJECTID as pid FROM Tracks' -nln gov_act -nlt MULTILINESTRING -t_srs 'EPSG:3857' -lco OVERWRITE=YES -lco GEOMETRY_NAME=geom PG: $<
	psql -c 'ALTER TABLE gov_act ADD way_length numeric;'
	psql -c 'UPDATE gov_act SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_act ADD CONSTRAINT gov_act_pid_unique UNIQUE (pid);'

### NSW

build/NPWS_RoadSection.zip:
	wget -O $@ https://datasets.seed.nsw.gov.au/dataset/09d4ab6b-39c1-4b4b-9193-2193022fe201/resource/8a8fef7f-9c11-4232-8ce9-b7e3287ea0bf/download/infrastructurenpwsroadsection.zip

# unfortunatly the supplied ID is not unique so it can't be used
govPgNSW_NPWS: build/NPWS_RoadSection.zip
	psql -c 'DROP TABLE IF EXISTS gov_nsw_npws;'
	rm -f /tmp/gov.geojson
	ogr2ogr -f GeoJSONSeq -mapFieldType Date=String /vsistdout /vsizip/$< | ./src/nsw_npws.js > /tmp/gov.geojson
	ogr2ogr -f PostgreSQL -nln gov_nsw_npws -unsetFid -lco GEOMETRY_NAME=geom -nlt MULTILINESTRING -t_srs 'EPSG:3857' PG: GeoJSONSeq:/tmp/gov.geojson
	rm -f /tmp/gov.geojson
	psql -c 'ALTER TABLE gov_nsw_npws ADD way_length numeric;'
	psql -c 'UPDATE gov_nsw_npws SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_nsw_npws ADD CONSTRAINT gov_nsw_npws_pid_unique UNIQUE (pid);'

build/NSW_ClassifiedFireTrail.geojson:
	esri2geojson --timeout 360 -v --jsonlines https://portal.spatial.nsw.gov.au/server/rest/services/NSW_Transport_Theme/FeatureServer/9 $@

build/NSW_ClassifiedFireTrail.fgb: build/NSW_ClassifiedFireTrail.geojson
	ogr2ogr -f FlatGeobuf $@ $<

build/NSW_ClassifiedFireTrail.shp: build/NSW_ClassifiedFireTrail.geojson
	ogr2ogr -f 'ESRI Shapefile' $@ $<

govPgNSW_FT: build/NSW_ClassifiedFireTrail.geojson
	psql -c 'DROP TABLE IF EXISTS gov_nsw_ft;'
	ogr2ogr -f PostgreSQL -unsetFid -sql 'SELECT objectid as pid FROM NSW_ClassifiedFireTrail' -nln gov_nsw_ft -nlt MULTILINESTRING -t_srs 'EPSG:3857' -lco OVERWRITE=YES -lco GEOMETRY_NAME=geom PG: $<
	psql -c 'ALTER TABLE gov_nsw_ft ADD way_length numeric;'
	psql -c 'UPDATE gov_nsw_ft SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_nsw_ft ADD CONSTRAINT gov_nsw_ft_pid_unique UNIQUE (pid);'

### VIC

govPgVIC: build/ll_gda94/sde_shape/whole/VIC/VMTRANS/layer/tr_road.shp
	psql -c 'DROP TABLE IF EXISTS gov_vic;'
	rm -f /tmp/gov.geojson
	ogr2ogr -f GeoJSONSeq -mapFieldType Date=String -where 'CLASS_CODE in (6, 7)' /vsistdout $< | ./src/vic.js > /tmp/gov.geojson
	ogr2ogr -f PostgreSQL -nln gov_vic -unsetFid -lco GEOMETRY_NAME=geom -nlt MULTILINESTRING -t_srs 'EPSG:3857' PG: GeoJSONSeq:/tmp/gov.geojson
	rm -f /tmp/gov.geojson
	psql -c 'ALTER TABLE gov_vic ADD way_length numeric;'
	psql -c 'UPDATE gov_vic SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_vic ADD CONSTRAINT gov_vic_pid_unique UNIQUE (pid);'

### QLD

govPgQLD: build/QSC_Extracted_Data_20210531_175628224000-36672/data.gdb
	psql -c 'DROP TABLE IF EXISTS gov_qld;'
	rm -f /tmp/gov.geojson
	ogr2ogr -f GeoJSONSeq -where 'ROADTYPE in (6, 7, 10)' /vsistdout $< | ./src/qld.js > /tmp/gov.geojson
	ogr2ogr -f PostgreSQL -nln gov_qld -unsetFid -lco GEOMETRY_NAME=geom -nlt MULTILINESTRING -t_srs 'EPSG:3857' PG: GeoJSONSeq:/tmp/gov.geojson
	rm -f /tmp/gov.geojson
	psql -c 'ALTER TABLE gov_qld ADD way_length numeric;'
	psql -c 'UPDATE gov_qld SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_qld ADD CONSTRAINT gov_qld_pid_unique UNIQUE (pid);'

### SA

build/SA_Roads.geojson.zip:
	wget -O $@ https://www.dptiapps.com.au/dataportal/Roads_geojson.zip

govPgSA: build/SA_Roads.geojson.zip
	psql -c 'DROP TABLE IF EXISTS gov_sa;'
	rm -f /tmp/gov.geojson
	ogr2ogr -f GeoJSONSeq -mapFieldType DateTime=String -where "class in ('TRK2', 'TRK4')" /vsistdout /vsizip/$</Roads_GDA2020.geojson | ./src/sa.js > /tmp/gov.geojson
	ogr2ogr -f PostgreSQL -nln gov_sa -unsetFid -lco GEOMETRY_NAME=geom -nlt MULTILINESTRING -t_srs 'EPSG:3857' PG: GeoJSONSeq:/tmp/gov.geojson
	rm -f /tmp/gov.geojson
	psql -c 'ALTER TABLE gov_sa ADD way_length numeric;'
	psql -c 'UPDATE gov_sa SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_sa ADD CONSTRAINT gov_sa_pid_unique UNIQUE (pid);'

### NT
build/nt.zip:
	wget -O $@ https://nt.gov.au/__data/assets/file/0006/164958/nt-government-controlled-road-maps.zip

govPgNT: build/nt.zip
	psql -c 'DROP TABLE IF EXISTS gov_nt;'
	ogr2ogr -f PostgreSQL -unsetFid -nln gov_nt -t_srs 'EPSG:3857' -lco GEOMETRY_NAME=geom PG: /vsizip/$</nt-government-controlled-road-maps.kmz
	psql -c 'ALTER TABLE gov_nt ADD way_length numeric;'
	psql -c 'UPDATE gov_nt SET way_length = ST_Length(geom);'

### TAS
build/tas: tasSources.txt
	mkdir -p $@
	wget --directory-prefix=$@ -i tasSources.txt
	parallel unzip -d $@ {} ::: build/tas/*.zip

govPgTas:
	rm -f build/tas/list_transport_segments.geojson
	ogrmerge.py -o build/tas/list_transport_segments.geojson build/tas/list_transport_segments_*.gdb -f GeoJSONSeq -single -src_layer_field_name file
	./src/tas.js < build/tas/list_transport_segments.geojson > /tmp/gov.geojson
	ogr2ogr -f PostgreSQL -nln gov_tas -unsetFid -lco GEOMETRY_NAME=geom -nlt MULTILINESTRING -t_srs 'EPSG:3857' PG: GeoJSONSeq:/tmp/gov.geojson
	rm -f /tmp/gov.geojson
	psql -c 'ALTER TABLE gov_tas ADD way_length numeric;'
	psql -c 'UPDATE gov_tas SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_tas ADD CONSTRAINT gov_tas_pid_unique UNIQUE (pid);'

### WA
build/wa.geojson:
	wget -O $@ 'https://portal-mainroads.opendata.arcgis.com/datasets/082e88d12c894956945ef5bcee0b39e2_17.geojson'

govPgWa: build/wa.geojson
	ogr2ogr -f PostgreSQL -unsetFid -sql 'SELECT OBJECTID AS pid, ROAD_NAME AS name FROM Road_Network' -nln gov_wa -nlt MULTILINESTRING -t_srs 'EPSG:3857' -lco OVERWRITE=YES -lco GEOMETRY_NAME=geom PG: $<
	psql -c 'ALTER TABLE gov_wa ADD way_length numeric;'
	psql -c 'UPDATE gov_wa SET way_length = ST_Length(geom);'
	psql -c 'ALTER TABLE gov_wa ADD CONSTRAINT gov_wa_pid_unique UNIQUE (pid);'

## Summary
counts:
	psql -c 'SELECT count(*) FROM gov_qld;'
	psql -c 'SELECT count(*) FROM gov_nsw_npws;'
	psql -c 'SELECT count(*) FROM gov_nsw_ft;'
	psql -c 'SELECT count(*) FROM gov_act;'
	psql -c 'SELECT count(*) FROM gov_vic;'
	psql -c 'SELECT count(*) FROM gov_sa;'
	psql -c 'SELECT count(*) FROM gov_nt;'
