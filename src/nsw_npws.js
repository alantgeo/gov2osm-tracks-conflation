#!/usr/bin/env node

const fs = require('fs')
const { Transform, pipeline } = require('readable-stream')
const ndjson = require('ndjson')
const hash = require('object-hash')

const subtype = {
  'Vehicle Trail': 'highway=track',
  'Driveway': 'highway=service,service=driveway',
  'Local Road': 'highway=unclassified',
  'Distributor Road': 'highway=unclassified',
  'Dormant Trail': 'highway=track',
  'Laneway': 'highway=service',
  'Road Continuity Line': 'highway=track'
}

const transform = new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(feature, encoding, callback) {
    const tags = {}

    tags.pid = hash(feature)

    tags.name = feature.properties.AssetName

    if (feature.properties.d_PushBike === 'True') {
      tags.bicycle = 'yes'
    }
    if (feature.properties.d_PushBike === 'False') {
      tags.bicycle = 'no'
    }

    if (feature.properties.d_HorseAll === 'True') {
      tags.horse = 'yes'
    }
    if (feature.properties.d_HorseAll === 'False') {
      tags.horse = 'no'
    }

    if (feature.properties.d_AssetTyp === '4WD') {
      tags['4wd_only'] = 'yes'
    }
    if (feature.properties.d_HorseAll === '2WD') {
      tags['4wd_only'] = 'no'
    }

    if (feature.properties.d_SubtypeC in subtype) {
      subtype[feature.properties.d_SubtypeC].split(',').forEach(tag => {
        const [key, value] = tag.split('=')
        tags[key] = value
      })
    }

    tags.note = feature.properties.Comments

    tags.source = 'NPWS Road Section'

    const osm = {
      type: 'Feature',
      properties: tags,
      geometry: feature.geometry
    }

    this.push(osm)

    callback()
  }
})

// stream in source ndjson, transfom then stream out
pipeline(
  fs.createReadStream('/dev/stdin'),
  ndjson.parse(),
  transform,
  ndjson.stringify(),
  fs.createWriteStream('/dev/stdout'),
  (err) => {
    if (err) {
      console.log(err)
      process.exit(1)
    } else {
      process.exit(0)
    }
  }
)
