#!/usr/bin/env node

const fs = require('fs')
const { Transform, pipeline } = require('readable-stream')
const ndjson = require('ndjson')

function toTitleCase(str) {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

const tagMap = {
  'TRAN_CLASS': {
    'National/State Highway': 'trunk',
    'Arterial Road': 'highway=primary',
    'Sub Arterial Road': 'highway=secondary',
    'Collector Road': 'highway=tertiary',
    'Local Road': 'highway=residential',
    'Access Road': 'highway=service',
    'Vehicular Track': 'highway=track',
    'AS2156 Track Class 1 (PWS)': 'highway=footway',
    'AS2156 Track Class 2 (PWS)': 'highway=footway',
    'AS2156 Track Class 3 (PWS)': 'highway=footway',
    'AS2156 Track Class 4 (PWS)': 'highway=footway',
    'AS2156 Track Class 5 (PWS)': 'highway=footway',
    'AS2156 Track Class 6 (PWS)': 'highway=footway',
    'Walking': 'highway=footway',
    'Ferry': 'route=ferry',
    'Bike': 'highway=cycleway',
    'Horse Trail': 'highway=bridleway',
    'Tramway': 'railway=tram',
    'Mall': 'highway=footway',
    'Railway': 'railway=rail',
    'Railway Siding': 'railway=rail'
  },
  'TRAFF_DIR': {
    'One': 'oneway=yes'
    // disabled because it seems to be used when the track isn't wide enough
    // not for true alternative oneway
    // 'Alternating': 'oneway=alternating'
  },
  'STATUS': {
    'Closed': 'access=no'
  },
  'SURFACE_TY': {
    '4WD required': 'surface=unpaved,4wd_only=yes',
    'Boardwalk': 'bridge=boardwalk',
    'Sealed': 'surface=paved',
    'Unsealed': 'surface=unpaved'
  },
  'TSEG_FEAT': {
    'Opening Bridge': 'bridge=yes',
    'Ford': 'ford=yes',
    'Tunnel': 'tunnel=yes',
    'Tunnel Dual Carraigeway': 'tunnel=yes',
    'Roundabout': 'junction=roundabout',
    'Bridge': 'bridge=yes',
    'Bridge Dual Carraigeway': 'bridge=yes'
  },
  'USER_TYPE': {
    'Restricted': 'motor_vehicle=private',
    'Private': 'motor_vehicle=private',
    'Authorised': 'motor_vehicle=private',
    'Public': 'motor_vehicle=yes'
  }
}

const keyMap = {
  'PRI_NAME': 'name',
  'SEC_NAME': 'alt_name'
}

const distinctUUIDs = {}

const transform = new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(feature, encoding, callback) {
    const tags = {}

    if (feature.properties.TRANS_TYPE === 'Rail' || feature.properties.TRANS_TYPE === 'Route') {
      // skip railways and routes
      callback()
      return
    }

    if (feature.properties.STATUS === 'Proposed') {
      // skip proposed roads for now
      callback()
      return
    }

    for (const [sourceKey, map] of Object.entries(tagMap)) {
      if (sourceKey in feature.properties) {
        if (feature.properties[sourceKey] in map) {
          map[feature.properties[sourceKey]].split(',').forEach(tag => {
            const [key, value] = tag.split('=')
            tags[key] = value
          })
        }
      }
    }

    for (const [sourceKey, targetKey] of Object.entries(keyMap)) {
      if (sourceKey in feature.properties) {
        tags[targetKey] = feature.properties[sourceKey]
      }
    }

    if (tags.bridge === 'yes' && feature.properties.BRIDGE_TUNNEL_NAME) {
      tags['bridge:name'] = feature.properties.BRIDGE_TUNNEL_NAME
    }
    if (tags.tunnel === 'yes' && feature.properties.BRIDGE_TUNNEL_NAME) {
      tags['tunnel:name'] = feature.properties.BRIDGE_TUNNEL_NAME
    }

    tags.pid = feature.properties.LIST_GUID.replace('{', '').replace('}', '') + '_' + feature.properties.file

    tags.source = 'TAS LIST Transport Segments'

    const osm = {
      type: 'Feature',
      properties: tags,
      geometry: feature.geometry
    }

    if (['track', 'service'].includes(tags.highway)) {
      if (tags.highway !== 'service' || (tags.highway === 'service' && tags.surface === 'unpaved')) {
        this.push(osm)
      }
    }

    callback()
  }
})

// stream in source ndjson, transfom then stream out
pipeline(
  fs.createReadStream('/dev/stdin'),
  ndjson.parse(),
  transform,
  ndjson.stringify(),
  fs.createWriteStream('/dev/stdout'),
  (err) => {
    if (err) {
      console.log(err)
      process.exit(1)
    } else {
      process.exit(0)
    }
  }
)
