#!/usr/bin/env node

const fs = require('fs')
const { Transform, pipeline } = require('readable-stream')
const ndjson = require('ndjson')

const surface = {
  'SEAL': 'surface=paved',
  'UNSE': 'surface=unpaved'
}

const structure = {
  'TUNL': 'tunnel=yes',
  'BRDG': 'bridge=yes',
  'CAUS': 'embankment=yes'
}

const access = {
  1: 'motor_vehicle=yes',
  2: 'motor_vehicle=private',
  3: 'motor_vehicle=private',
  4: 'motor_vehicle=no'
}

function toTitleCase(str) {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

// http://www.location.sa.gov.au/lms/Reports/ReportMetadata.aspx?p_no=558&pu=y
const transform = new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(feature, encoding, callback) {
    const tags = {}

    tags.pid = feature.properties.persistentid

    tags.highway = 'track'

    if (feature.properties.name && feature.properties.roadtype) {
      tags.name = [toTitleCase(feature.properties.name), toTitleCase(feature.properties.roadtype)].join(' ')
    }

    if (feature.properties.surface in surface) {
      const [key, value] = surface[feature.properties.surface].split('=')
      tags[key] = value
    }

    if (feature.properties.routenum) {
      tags.ref = feature.properties.routenum
    }

    if (feature.properties.ontype in structure) {
      const [key, value] = structure[feature.properties.ontype].split('=')
      tags[key] = value
    }

    if (feature.properties.roadusetype in access) {
      const [key, value] = access[feature.properties.roadusetype].split('=')
      tags[key] = value
    }

    tags.source = 'SA Roads'

    const osm = {
      type: 'Feature',
      properties: tags,
      geometry: feature.geometry
    }

    this.push(osm)

    callback()
  }
})

// stream in source ndjson, transfom then stream out
pipeline(
  fs.createReadStream('/dev/stdin'),
  ndjson.parse(),
  transform,
  ndjson.stringify(),
  fs.createWriteStream('/dev/stdout'),
  (err) => {
    if (err) {
      console.log(err)
      process.exit(1)
    } else {
      process.exit(0)
    }
  }
)
