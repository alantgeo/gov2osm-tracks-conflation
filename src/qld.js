#!/usr/bin/env node

const fs = require('fs')
const { Transform, pipeline } = require('readable-stream')
const ndjson = require('ndjson')
const hash = require('object-hash')

function toTitleCase(str) {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

const transform = new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(feature, encoding, callback) {
    const tags = {}

    tags.pid = hash(feature)

    if (feature.properties.STREET === 'PROPOSED ROAD') {
      tags['proposed:highway'] = 'yes'
    } else {
      tags.highway = 'track'
    }

    if (
      feature.properties.STREET &&
      feature.properties.STREET.toUpperCase() !== 'PROPOSED ROAD' &&
      feature.properties.STREET.toUpperCase() !== 'UNNAMED ROAD' &&
      feature.properties.STREET.toUpperCase() !== 'TRACK'
    ) {
      tags.name = toTitleCase(feature.properties.STREET)
    }

    tags.source = 'QLD Baseline Roads and Tracks'

    const osm = {
      type: 'Feature',
      properties: tags,
      geometry: feature.geometry
    }

    if (!osm.properties['proposed:highway']) {
      // skip proposed roads
      this.push(osm)
    }

    callback()
  }
})

// stream in source ndjson, transfom then stream out
pipeline(
  fs.createReadStream('/dev/stdin'),
  ndjson.parse(),
  transform,
  ndjson.stringify(),
  fs.createWriteStream('/dev/stdout'),
  (err) => {
    if (err) {
      console.log(err)
      process.exit(1)
    } else {
      process.exit(0)
    }
  }
)
