#!/usr/bin/env node

const fs = require('fs')
const { Transform, pipeline } = require('readable-stream')
const ndjson = require('ndjson')

const classCode = {
  0: 'highway=motorway', // freeway
  1: 'highway=highway', // highway
  2: 'highway=primary', // arterial
  3: 'highway=secondary', // sub-arterial
  4: 'highway=tertiary', // collector
  5: 'highway=residential', // local
  6: 'highway=track', // 2WD track
  7: 'highway=track', // 4WD track
  9: 'proposed:highway=yes', // proposed
  11: 'highway=footway', // walking track
  12: 'highway=cycleway', // bicycle path
  14: 'route=ferry,motor_vehicle=yes' // ferry route
}

const roadRestrictions = {
  1: 'motor_vehicle=private', // maintenance vehicles only
  2: 'note=seasonal closure', // subject to seasonal closure
  4: 'motor_vehicle=no', // permanently closed
  5: 'motor_vehicle=private', // private access
  7: 'note=dry weather only' // dry weather only
}

const transform = new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(feature, encoding, callback) {
    const tags = {}

    tags.pid = feature.properties.PFI

    tags.name = feature.properties.EZIRDNMLBL === 'Unnamed' ? null : feature.properties.EZIRDNMLBL

    if (feature.properties.CLASS_CODE in classCode) {
      classCode[feature.properties.CLASS_CODE].split(',').forEach(tag => {
        const [key, value] = tag.split('=')
        tags[key] = value
      })
    } else {
      tags.highway = 'road'
    }

    if (feature.properties.CLASS_CODE === 7) {
      tags['4wd_only'] = 'yes'
    }

    if (feature.properties.DIR_CODE === 'F') {
      tags.oneway = 'yes'
    }
    if (feature.properties.DIR_CODE === 'R') {
      tags.oneway = '-1'
    }
    if (feature.properties.DIR_CODE === 'B') {
      tags.oneway = 'no'
    }

    if (feature.properties.RESTRICTN in roadRestrictions) {
      const [key, value] = roadRestrictions[feature.properties.RESTRICTN].split('=')
      tags[key] = value
    }

    if (feature.properties.ROAD_SEAL === 1) {
      tags.surface = 'paved'
    }
    if (feature.properties.ROAD_SEAL === 2) {
      tags.surface = 'unpaved'
    }

    tags.source = 'Vicmap Transport Roads'

    const osm = {
      type: 'Feature',
      properties: tags,
      geometry: feature.geometry
    }

    if (!osm.properties['proposed:highway']) {
      // skip proposed roads
      this.push(osm)
    }

    callback()
  }
})

// stream in source ndjson, transfom then stream out
pipeline(
  fs.createReadStream('/dev/stdin'),
  ndjson.parse(),
  transform,
  ndjson.stringify(),
  fs.createWriteStream('/dev/stdout'),
  (err) => {
    if (err) {
      console.log(err)
      process.exit(1)
    } else {
      process.exit(0)
    }
  }
)
